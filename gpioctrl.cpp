#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <ctime>
using std::cout;
using std::string;
using std::ifstream;
using std::stringstream;

int pinlist[] = { 4, 17, 21, 22, 18, 23, 24, 25 };

int main();
void setpindirections(int *pindirections);
void setpinvalues(int *pinvalues);
void setoldpinvalues(int *pinvalues, int *oldpinvalues);
void comparepinvalues(int *oldpinvalues, int *pinvalues);

string InttoString(int tint);
int getPinVal(int pinnumber);

int main()
{
	int pincount = sizeof(pinlist)/sizeof(pinlist[0]);
	int pinval;

	cout << "GPIO Pin Service\n" << "(C)Steve Verhagen 2013\n"
	<< "**********************************************\n";

	cout << "Total pins available: " << pincount << "\n";
	cout << "---\n";
	
	int pindirections[pincount];
	setpindirections(pindirections);

	int oldpinvalues[pincount];
	int pinvalues[pincount];
	setpinvalues(oldpinvalues);

	//Get pin counts:
	int inpins = 0;
	int outpins = 0;
	for ( int i=0; i < pincount; i++ )
	{
		if ( pindirections[i] == 1 )
		{
			inpins++;
		} else {
			outpins++;
		}
	}

	cout << "Input Pins:" << inpins << "\n";
	cout << "Output Pins:" << outpins << "\n";
	cout << "**********************************************\n";
	
	if ( inpins < 1 )
	{
		cout << "No input pins activated--quitting.\n\n";
		return 1;
	}

	while (1) {
		setpinvalues(pinvalues);
		comparepinvalues(oldpinvalues, pinvalues);
		setoldpinvalues(pinvalues, oldpinvalues);
		sleep(1);
		cout << ".";
	}
	return 1;
}

void setpindirections(int *pindirections)
{
	string thispinpath;
	ifstream pinstream;

	string thisdirection;

	int pincount = sizeof(pinlist)/sizeof(pinlist[0]);
	for ( int i = 0; i< pincount; i++ )
	{
		//cout << "current pin:  " << pinlist[i] << "\n";
		thispinpath = "/sys/class/gpio/gpio" + InttoString(pinlist[i]) + "/direction";
		//cout << thispinpath << "\n";
		pinstream.open(thispinpath.c_str(), ifstream::in);
		pinstream >> thisdirection;
			// pin directions:  1 = in/2 = out
			if ( thisdirection == "out" )
			{
				pindirections[i] = 2;
			} else {
				pindirections[i] = 1;
			}
		pinstream.close();
	}
		
}

void setpinvalues(int *pinvalues)
{
	string thispinpath;
	ifstream pinstream;

	string thisvalue;

	int pincount = sizeof(pinlist)/sizeof(pinlist[0]);
	for ( int i = 0; i< pincount; i++ )
	{
		//cout << "current pin:  " << pinlist[i] << "\n";
		thispinpath = "/sys/class/gpio/gpio" + InttoString(pinlist[i]) + "/value";
		//cout << thispinpath << "\n";
		pinstream.open(thispinpath.c_str(), ifstream::in);
		pinstream >> thisvalue;
			if ( thisvalue == "1" )
			{
				pinvalues[i] = 1;
			} else {
				pinvalues[i] = 0;
			}
		pinstream.close();
	}
		
}

void setoldpinvalues(int *pinvalues, int *oldpinvalues)
{
	int pincount = sizeof(pinlist)/sizeof(pinlist[0]);
	for (int i = 0; i < pincount; i++)
	{
		oldpinvalues[i] = pinvalues[i];
	}
}

string InttoString(int tint)
{
   stringstream tstring;
   tstring << tint;
   return tstring.str();
}

int getPinVal(int pinnumber)
{
	string thispinpath;
	ifstream pinstream;
	string thisvalue;

	thispinpath = "/sys/class/gpio/gpio" + InttoString(pinnumber) + "/value";

	pinstream.open(thispinpath.c_str(), ifstream::in);
	pinstream >> thisvalue;
	pinstream.close();
	return atoi(thisvalue.c_str());
}

void comparepinvalues(int *oldpinvalues, int *pinvalues)
{
	string scripttorun;	
		
	int pincount = sizeof(pinlist)/sizeof(pinlist[0]);
	for (int i=0; i< pincount; i++)
	{
		if ( oldpinvalues[i] != pinvalues[i] )
		{
			cout << "pin change (" << pinlist[i] << "): " << oldpinvalues[i] << " to " << pinvalues[i] << "\n";
			if ( pinvalues[i] == 0 ) {
				//Run off script for pin:
				scripttorun = "/etc/gpioctrl/scripts/gpio" + InttoString(pinlist[i]) + "_off.sh";
			} else {
				//Run on script for pin:
				scripttorun = "/etc/gpioctrl/scripts/gpio" + InttoString(pinlist[i]) + "_on.sh";
			}
			cout << "Running script: " << scripttorun << "\n";
			system(scripttorun.c_str());
		}
	}
}
