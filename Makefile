all:
	g++ gpioctrl.cpp -o gpioctrl
	@echo Now type 'sudo make install' to install.

clean:
	rm gpioctrl

install:
	@echo Copy init script...
	cp init/gpioact /etc/init.d/gpioact
	sudo /usr/sbin/update-rc.d -f gpioact defaults
	
	@echo Copy gpio tool to /usr/bin...
	cp gpio /usr/bin/gpio
	@echo 
	@echo Copy gpioctrl service /usr/bin...
	killall gpioctrl || echo
	cp gpioctrl /usr/bin/gpioctrl
	@echo
	@echo Create settings directory /etc/gpioctrl...
	mkdir -p /etc/gpioctrl/scripts
	cp scripts/*.sh /etc/gpioctrl/scripts
	
	@echo 
	

